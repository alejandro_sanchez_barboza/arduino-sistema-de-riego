#include <Servo.h>
Servo servo_motor;

// Variables para la compuerta
int nivelAgua = A0;
int valorAgua = 0;
int nivelAlto = false;
int nivelAguaMedio = 512;


int ledRojo = 4;
int ledAzul = 5;
int Senluz = A5;

int pulsadorRojo = 7;
int pulsadorAmarillo = 8;


int cuentaDia =0;
int cuentaRiego = 0;


int limiteDN = 800;

int buzzer = 6;

// Definimos los pines donde tenemos conectadas las bobinas
#define IN1  12
#define IN2  9
#define IN3  10
#define IN4  11
// para el motor 
// Secuencia de pasos (par máximo)
int paso [4][4] =
{
  {1, 1, 0, 0},
  {0, 1, 1, 0},
  {0, 0, 1, 1},
  {1, 0, 0, 1}
};



void setup() {
  pinMode(Senluz, INPUT);
  pinMode(ledAzul,OUTPUT);
  pinMode(ledRojo, OUTPUT);

// para el pulsador rojo
  pinMode(pulsadorRojo,INPUT);

// para el pulsador amarillo
  pinMode(pulsadorAmarillo,INPUT);

  // Inicializacion para el riego
  servo_motor.attach(2);
  servo_motor.write(0);
//sonido
pinMode(buzzer, OUTPUT);   // sets the pin as output
}

void loop() {
  int valueBoton = digitalRead(pulsadorAmarillo);
  if (valueBoton == LOW) {
    //digitalWrite(ledRojo, HIGH); 
  riego();
  generador();
  seguridad();  
  //chunche();
  }
}

void riego(){
  //
  int value = analogRead(Senluz); 
   if(value < limiteDN){
    //digitalWrite(ledRojo, HIGH); 
       //cuentaAlimento = 0;
       cuentaDia = 0;
       cuentaRiego = 0;
  }
  else{
    if(cuentaRiego <= 1){
      int valueBoton = digitalRead(pulsadorAmarillo);
      
      if (valueBoton == LOW) {
    digitalWrite(ledAzul, HIGH);
    servo_motor.write(0); // Abre compuerta
    delay(500);
      }
      valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
    servo_motor.write(30);
    delay(500);
      }
      valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
    servo_motor.write(60);
    delay(500);
      }
     valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
    servo_motor.write(90);
    delay(500);
      }
      valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
    servo_motor.write(120);
    delay(500);
      }
      valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
     servo_motor.write(150);
    delay(500);
      }
      valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
     servo_motor.write(180);
    delay(500);
      }
      valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
    servo_motor.write(0);
      }
    digitalWrite(ledAzul, LOW);
     cuentaRiego = cuentaRiego + 1;
    }

    
  }
  
}
bool generador(){
  int valueBoton = digitalRead(pulsadorRojo);
   
  if (valueBoton == HIGH) {
    //digitalWrite(ledRojo, HIGH); 
   for(int j=1; j<1000; j++){ 
    for (int i = 0; i < 4; i++)
    {
      int valueBoton = digitalRead(pulsadorAmarillo);
      if (valueBoton == LOW) {
      digitalWrite(IN1, paso[i][0]);
      digitalWrite(IN2, paso[i][1]);
      digitalWrite(IN3, paso[i][2]);
      digitalWrite(IN4, paso[i][3]);
      delay(2);
      }
    }
  
        //delay(3000);

      }
}
}

bool seguridad(){
  // Motor al puerto 2 Digital
  // Regulador al puerto A0 Analogo
  valorAgua = analogRead(nivelAgua);

  int valueBoton = digitalRead(pulsadorAmarillo);
     // if (valueBoton == LOW) {
  if(valorAgua > nivelAguaMedio + 20 && valueBoton == LOW){
    digitalWrite(ledRojo, HIGH);
    // que suene
analogWrite(buzzer,300); //emite sonido
      //}
    
  }
  else{
    // que deje de sonar
     digitalWrite(buzzer, LOW); //deja de emitir
    digitalWrite(ledRojo, LOW); 
    nivelAlto = false;
  }
  return nivelAlto;
  }


